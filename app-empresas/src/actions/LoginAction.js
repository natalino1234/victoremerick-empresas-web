import { Sessao } from "../utils/Sessao/Sessao";

export const submitLoginAction = (data, paginaInicial) => {
    let url = 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in';
    fetch(url, {
        method: "POST",
        headers: {
            'Content-type':'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        Sessao.status = response.status;
        if(response.status === 200){
            Sessao.logado = true;
            Sessao.dados["access-token"] = response.headers.get("access-token");
            Sessao.dados["client"] = response.headers.get("client");
            Sessao.dados["uid"] = response.headers.get("uid");
        }
        return response.json();
    })
    .then(json => {
        switch(Sessao.status){
            case 200:
                paginaInicial.setState({logado : json.success});
                break;
            case 500:
                paginaInicial.mostrarMensagem("Serviço indisponível, contate o administrador.");
                break;
            default:
                paginaInicial.mostrarMensagem("E-mail ou senha incorretos.");
                break;
        }
    });
};