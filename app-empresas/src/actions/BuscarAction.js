import { Sessao } from "../utils/Sessao/Sessao";

export const BuscarAction = (paginaCallback, nome = null, codigo = null) => {
    let url = 'http://empresas.ioasys.com.br/api/v1/enterprises';
    if(nome !== null && codigo !== null){
        url += "?enterprise_types="+codigo+"&name="+nome;
    }
    if(Sessao.logado){
        let data = Sessao.dados;

        fetch(url, {
            method: "GET",
            headers: {
                'Content-type':'application/json',
                'access-token' : data["access-token"],
                'client' : data.client,
                'uid' : data.uid
            }
        })
        .then(response => {
            return response.json();
        })
        .then(json => {
            paginaCallback.setState({itens : json.enterprises});
        });
    }else{
        window.location.reload();
    }
};