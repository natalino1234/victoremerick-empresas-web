import { Sessao } from "../utils/Sessao/Sessao";

export const DetalharAction = (paginaCallback, id) => {
    let url = 'http://empresas.ioasys.com.br/api/v1/enterprises/'+id;
    if(Sessao.logado){
        let data = Sessao.dados;

        fetch(url, {
            method: "GET",
            headers: {
                'Content-type':'application/json',
                'access-token' : data["access-token"],
                'client' : data.client,
                'uid' : data.uid
            }
        })
        .then(response => {
            if(response.status === 404){
                paginaCallback.setState({empresa : 0});
            }
            return response.json();
        })
        .then(json => {
            if(paginaCallback.state.empresa === null){
                paginaCallback.setState({empresa : json.enterprise});
            }
        });
    }else{
        window.location.reload();
    }
};