import Buscar from "../pages/Buscar/Buscar";
import Detalhar from "../pages/Detalhar/Detalhar";

const routes = [
    {
        path: "/",
        component: Buscar,
        exact: true
    },
    {
        path: "/Buscar",
        component: Buscar,
        exact: true
    },
    {
        path: "/Detalhar",
        component: Detalhar,
        exact: true
    }

];

export default routes;