import React, {Component} from 'react';
import './App.css';
import routes from './utils/routes';
import {Route} from 'react-router-dom'
import Login from './pages/Login/Login';
import { Sessao } from './utils/Sessao/Sessao';

class App extends Component{

  constructor(props){
    super(props);
    this.state = {
      logado : false,
      statusMensagem: false,
      textoMensagem : ""
    };

    this.mostrarMensagem = this.mostrarMensagem.bind(this);
  }

  mostrarMensagem(mensagem){
    console.log(mensagem);
    this.setState({
      statusMensagem: true,
      textoMensagem: mensagem
    });
  }

  render(){
    if(!Sessao.logado){
      return (
        <div className="App">
          <Login parent={this}></Login>
        </div>);
    }else{
      return (
        <div className="App">
          {routes.map((value, key)=>{
            return <Route key={key} path={value.path} component={value.component} exact={value.exact}></Route>
          })}
        </div>
      );
    }
  }
}

export default App;
