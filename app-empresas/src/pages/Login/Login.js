import React, {Component} from 'react';
import LoginForm from '../../components/LoginForm/LoginForm';
import './Login.css';
import logo from './imgs/logo-home.png';
import { Sessao } from '../../utils/Sessao/Sessao';

class Login extends Component{

    constructor(props){
        super(props);
        this.getMensagemErro = this.getMensagemErro.bind(this);
    }

    getMensagemErro(){
        if(Sessao.logado){
            return;   
        }
        if(this.props.parent.state.statusMensagem){
            return(<p className="error">{this.props.parent.state.textoMensagem}</p>);
        }else{
            return;
        }
    }

    render(){
        return(
            <main>
                <section>
                    <img className="logo" src={logo} alt="Logo da IOASYS"/>
                    <h1 className="BEM-VINDO-AO-EMPRESA">BEM-VINDO AO EMPRESAS</h1>
                    <p className="Lorem-ipsum-dolor-si">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                    {this.getMensagemErro()}
                    <LoginForm paginaInicial={this.props.parent}></LoginForm>
                </section>
            </main>
        );
    }
}

export default Login;