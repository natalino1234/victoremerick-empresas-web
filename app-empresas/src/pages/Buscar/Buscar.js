import React, {Component} from 'react';
import './Buscar.css';
import logo from './imgs/logo-nav.png';
import icon_search from './imgs/ic-search-copy.svg';
import icon_close from './imgs/ic-close.svg';
import Item from '../../components/item_busca/Item';
import {Link} from 'react-router-dom'
import { BuscarAction } from '../../actions/BuscarAction';
import queryString from 'query-string';

class Buscar extends Component{
    constructor(props){
        super(props);
        
        this.onClickEvent_AtivarBusca = this.onClickEvent_AtivarBusca.bind(this);
        this.onClickEvent_DesativarBusca = this.onClickEvent_DesativarBusca.bind(this);
        this.onKeyUpEvent_Buscar = this.onKeyUpEvent_Buscar.bind(this);
        this.onChangeEvent_Buscar = this.onChangeEvent_Buscar.bind(this);
        this.getConteudo = this.getConteudo.bind(this);

        const values = queryString.parse(this.props.location.search);
        if(this.props.location.search.length === 0){
            this.state = {
                buscar: false,
                buscar_class: "busca",
                busca_content: "",
                itens : []
            };
            BuscarAction(this);
        }else{
            this.state = {
                buscar: true,
                buscar_class: "buscando",
                busca_content: values.nome+" "+values.id,
                itens : []
            };
            BuscarAction(this, values.nome, values.id);
        }
    }

    onClickEvent_AtivarBusca(e){
        this.setState({
                buscar_class: "buscando",
                buscar: true
            });
    }

    onClickEvent_DesativarBusca(e){
        this.setState({
                buscar_class: "busca",
                buscar: false
            });
    }

    getConteudo(){
        if(this.state.buscar){
            return (
                <div>
                    {this.state.itens.map((value, index) => {
                        let tipo = value.enterprise_type;
                        return (
                            <Link to={"/Detalhar?id="+value.id}  key={index}>
                                <Item
                                    nome={value.enterprise_name} 
                                    imagem={value.photo}
                                    tipo={tipo.enterprise_type_name} 
                                    local={value.city+", "+value.country}/>
                            </Link>
                        );
                    })}
                </div>
            );
        }else{
            return <p className="Clique-na-busca-para">Clique na busca para iniciar</p>;
        }
    }

    onKeyUpEvent_Buscar(e){
        if(e.keyCode === 13){
            let texto = e.target.value.split(" ");
            if(texto.length === 2){
                let nome = texto[0];
                let id = texto[1];
                BuscarAction(this,nome, id);
            }else{
                BuscarAction(this);
            }
        }
    }
    onChangeEvent_Buscar(e){
        if(e.keyCode !== 13){
            this.setState({busca_content: e.target.value});
        }
    }

    render(){
        return(
            <div className="BuscarPage">
                <nav className={"background "+this.state.buscar_class}>
                    <img className="logo_nav" src={logo} alt="Logo da IOASYS"/>
                    <label>
                        <img className="ic_search-copy" src={icon_search} alt="Buscar" onClick={this.onClickEvent_AtivarBusca}></img>
                        <input className="input-buscar" name="buscar" placeholder="Pesquisar" value={this.state.busca_content} values={this.state.busca_content} onChange={this.onChangeEvent_Buscar} onKeyUp={this.onKeyUpEvent_Buscar}></input>
                        <img className="ic_close" src={icon_close} alt="Fechar" onClick={this.onClickEvent_DesativarBusca}></img>
                    </label>
                </nav>
                <main>
                    <article className="conteudo">
                        {this.getConteudo()}
                    </article>
                </main>
            </div>
        );
    }
}

export default Buscar;