import React, {Component} from 'react';
import '../Buscar/Buscar.css';
import './Detalhar.css';
import e1 from './imgs/img-e-1-lista.svg';
import logo from './imgs/logo-nav.png';
import icon_search from './imgs/ic-search-copy.svg';
import icon_close from './imgs/ic-close.svg';
import queryString from 'query-string';
import { Redirect } from 'react-router'
import { DetalharAction } from '../../actions/DetalharAction';

class Detalhar extends Component{

    constructor(props){
        super(props);

        this.state = {
            buscar: false,
            buscar_class: "busca",
            redirect: false,
            destino: "",
            empresa: null
        };
        this.onClickEvent_AtivarBusca = this.onClickEvent_AtivarBusca.bind(this);
        this.onClickEvent_DesativarBusca = this.onClickEvent_DesativarBusca.bind(this);
        this.onKeyUpEvent_Buscar = this.onKeyUpEvent_Buscar.bind(this);
        this.onChangeEvent_Buscar = this.onChangeEvent_Buscar.bind(this);
        this.getDetalhes = this.getDetalhes.bind(this);
        this.redirect = this.redirect.bind(this);

        const values = queryString.parse(this.props.location.search);
        DetalharAction(this, values.id);
    }

    onClickEvent_AtivarBusca(e){
        this.setState({
                buscar_class: "buscando",
                buscar: true
            });
    }

    onClickEvent_DesativarBusca(e){
        this.setState({
                buscar_class: "busca",
                buscar: false
            });
    }

    onKeyUpEvent_Buscar(e){
        if(e.keyCode === 13){
            let texto = e.target.value.split(" ");
            if(texto.length === 2){
                let nome = texto[0];
                let id = texto[1];
                this.setState({
                    destino: "/Buscar?nome="+nome+"&id="+id,
                    redirect: true
                });
            }else{
                this.setState({
                    destino: "/Buscar",
                    redirect: true
                });
            }
        }
        console.log(this.state);
    }
    onChangeEvent_Buscar(e){
        if(e.keyCode !== 13){
            this.setState({busca_content: e.target.value});
        }
    }

    redirect(){
        if(this.state.redirect){
            return <Redirect to={this.state.destino}/>
        }
        return "";
    }

    getDetalhes(){
        if(this.state.empresa === null){
            return <p>Carregando...</p>
        }else if(this.state.empresa === 0){
            return <p>Empresa não encontrada.</p>;
        }else{
            console.log(this.state.empresa);
            return(
                <section className="detalhar">
                    <img src={(this.state.empresa.photo!==null)?"http://empresas.ioasys.com.br"+this.state.empresa.photo:e1} alt={"Logo da "+this.state.empresa.enterprise_name}></img>
                    <p className="Lorem-ipsum-dolor-si">
                        {this.state.empresa.description}
                    </p>
                </section>
            );
        }
    }

    render(){
        return(
            <div className="BuscarPage">
                <nav className={"background "+this.state.buscar_class}>
                    <img className="logo_nav" src={logo} alt="Logo da IOASYS"/>
                    <label>
                        <img className="ic_search-copy" src={icon_search} alt="Buscar" onClick={this.onClickEvent_AtivarBusca}></img>
                        <input className="input-buscar" name="buscar" placeholder="Pesquisar" values={this.state.busca_content} onChange={this.onChangeEvent_Buscar} onKeyUp={this.onKeyUpEvent_Buscar}></input>
                        {this.redirect()}
                        <img className="ic_close" src={icon_close} alt="Fechar" onClick={this.onClickEvent_DesativarBusca}></img>
                    </label>
                </nav>
                <main>
                    {this.getDetalhes()}
                </main>
            </div>
        );
    }
}

export default Detalhar;
