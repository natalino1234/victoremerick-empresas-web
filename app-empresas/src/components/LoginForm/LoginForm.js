import {reduxForm, Field} from 'redux-form'
import { connect } from 'react-redux';
import React from 'react';
import {submitLoginAction} from '../../actions/LoginAction';
import cadeado_icon from './imgs/ic-cadeado.svg';
import email_icon from './imgs/ic-email.svg';

const LoginFormFunc = props => {

    const {handleSubmit} = props;

    const submit = (data, submitLoginAction) => {
        submitLoginAction(data, props.paginaInicial);
    };

    return(
        <form onSubmit={handleSubmit((fields) => submit(fields, submitLoginAction))}>
            <label for="usuario">
                <img src={email_icon} alt="E-mail do Usuário"/>
                <Field className="E-mail" component="input" name="email" type="email" placeholder="E-mail" />
            </label>
            <label for="password">
                <img src={cadeado_icon} alt="Senha do Usuário"/>
                <Field className="Senha" component="input" name="password" type="password" placeholder="Senha"/>
            </label>
            <button className="Rectangle-26 ENTER" type="submit">Entrar</button>
        </form>
    );
}

const LoginForm = (reduxForm({
    form:'formLogin'
}))(LoginFormFunc);

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {submitLoginAction})(LoginForm);