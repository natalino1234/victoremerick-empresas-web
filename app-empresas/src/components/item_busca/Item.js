import React, {Component} from 'react';
import './Item.css';
import e1 from './imgs/img-e-1-lista.svg';

class Item extends Component{

    constructor(props){
        super(props);
        this.checarImagemOnline = this.checarImagemOnline.bind(this);
    }

    checarImagemOnline(imageUrl){
        var img = new Image();
        img.src = imageUrl;
        return (img.height>0);
    }

    render(){
        return(
            <section className="item">
                <img src={(this.props.imagem!==null)?"http://empresas.ioasys.com.br"+this.props.imagem:e1} alt={"Logo da "+this.props.nome}></img>
                <div className="dados">
                    <h2>{this.props.nome}</h2>
                    <h3>{this.props.tipo}</h3>
                    <h4>{this.props.local}</h4>
                </div>
            </section>
        );
    }
}

export default Item;